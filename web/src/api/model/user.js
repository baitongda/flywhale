import http from "@/utils/request"

export default {
	login: {
		url: '/login/doLogin',
		name: "登录获取TOKEN",
		post: async function(data={}) {
			return await http.post(this.url, data);
		}
	},
  	getInfo: {
    	url: '/login/getMenu',
		name: "获取菜单",
		get: async function(data={}) {
			return await http.get(this.url, data);
		}
	},
	logout: {
		url: '/login/logout',
		name: "退出登录",
		get: async function(data={}) {
			return await http.get(this.url, data);
		}
	},
	userList: {
		url: '/user/index',
		name: "获取用户列表",
		get: async function(data={}) {
			return await http.get(this.url, data);
		}
	},
	add: {
		url: '/user/add',
		name: "添加用户列表",
		post: async function(data={}) {
			return await http.post(this.url, data);
		}
	},
	edit: {
		url: '/user/edit',
		name: "编辑用户列表",
		post: async function(data={}) {
			return await http.post(this.url, data);
		}
	},
	del: {
		url: '/user/del',
		name: "删除用户",
		get: async function(data={}) {
			return await http.get(this.url, data);
		}
	},
}
