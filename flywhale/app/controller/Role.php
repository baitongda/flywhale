<?php

namespace app\controller;

use app\service\RoleService;
use think\annotation\Inject;

class Role extends Base
{
    /**
     * @Inject()
     * @var RoleService
     */
    protected $roleService;

    public function index()
    {
        $param = input('param.');

        $res = $this->roleService->getRoleList($param);

        return json(pageReturn($res));
    }

    public function showNode()
    {
        return json(getNodeData());
    }

    public function add()
    {
        if (request()->isPost()) {

            $param = input('post.');

            $res = $this->roleService->addRole($param);
            return json($res);
        }
    }

    public function edit()
    {
        if (request()->isPost()) {

            $param = input('post.');

            $res = $this->roleService->editRole($param);
            return json($res);
        }
    }

    public function del()
    {
        $id = input('param.id');

        $res = $this->roleService->delRole($id);
        return json($res);
    }
}