<?php

namespace app\controller;

use app\service\DictCateService;
use think\annotation\Inject;

class Dictcate extends Base
{
    /**
     * @Inject()
     * @var DictCateService
     */
    protected $dictCateService;

    public function index()
    {
        $res = $this->dictCateService->getDictCateTree();
        return json($res);
    }

    public function all()
    {
        $res = $this->dictCateService->getAllDictCate();
        return json($res);
    }

    public function add()
    {
        if (request()->isPost()) {

            $param = input('post.');

            $res = $this->dictCateService->addDictCate($param);
            return json($res);
        }
    }

    public function edit()
    {
        if (request()->isPost()) {

            $param = input('post.');

            $res = $this->dictCateService->editDictCate($param);
            return json($res);
        }
    }

    public function del()
    {
        $id = input('param.id');

        $res = $this->dictCateService->delDictCate($id);
        return json($res);
    }
}