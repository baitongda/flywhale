<?php

namespace app\model;

use think\facade\Log;

class Form extends BaseModel
{
    /**
     * 获取设计表的列表
     * @param $limit
     * @param $where
     * @return array
     */
    public function getFormList($limit, $where)
    {
        try {

            $list = $this->where($where)->order('id', 'desc')->paginate($limit);
        }catch (\Exception $e) {
            Log::error('获取表单列表错误: ' . $e->getMessage() . PHP_EOL  . $e->getTraceAsString());
            return dataReturn(-1, $e->getMessage());
        }

        return dataReturn(0, 'success', $list);
    }

    /**
     * 添加表单
     * @param $param
     * @return array
     */
    public function addForm($param)
    {
        try {

            $info = $this->where('title', $param['title'])->find();
            if (!empty($info)) {
                return dataReturn(-2, '该表单已经存在');
            }

            $param['create_time'] = date('Y-m-d H:i:s');
            $this->insert($param);
        }catch (\Exception $e) {
            Log::error('添加表单失败: ' . $e->getMessage() . PHP_EOL  . $e->getTraceAsString());
            return dataReturn(-1, $e->getMessage());
        }

        return dataReturn(0, '添加成功');
    }

    /**
     * 编辑表单
     * @param $param
     * @return array
     */
    public function editForm($param)
    {
        try {

            $info = $this->where('title', $param['title'])->where('id', '<>', $param['id'])->find();
            if (!empty($info)) {
                return dataReturn(-2, '该表单已经存在');
            }

            $param['update_time'] = date('Y-m-d H:i:s');
            $this->where('id', $param['id'])->update($param);
        }catch (\Exception $e) {
            Log::error('编辑表单失败: ' . $e->getMessage() . PHP_EOL  . $e->getTraceAsString());
            return dataReturn(-1, $e->getMessage());
        }

        return dataReturn(0, '编辑成功');
    }

    /**
     * 删除表单
     * @param $id
     * @return array
     */
    public function delForm($id)
    {
        try {

            $this->where('id', $id)->delete();
        }catch (\Exception $e) {
            Log::error('删除表单失败: ' . $e->getMessage() . PHP_EOL  . $e->getTraceAsString());
            return dataReturn(-1, $e->getMessage());
        }

        return dataReturn(0, '删除成功');
    }
}