/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50737
 Source Host           : localhost:3306
 Source Schema         : flylc

 Target Server Type    : MySQL
 Target Server Version : 50737
 File Encoding         : 65001

 Date: 09/05/2022 08:07:45
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for fc_dept
-- ----------------------------
DROP TABLE IF EXISTS `fc_dept`;
CREATE TABLE `fc_dept` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '部门id',
  `name` varchar(155) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '部门名称',
  `pid` int(11) DEFAULT '0' COMMENT '上级部门id',
  `status` tinyint(2) DEFAULT '1' COMMENT '1:启用 2:禁用',
  `desc` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '部门描述',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='部门表';

-- ----------------------------
-- Records of fc_dept
-- ----------------------------
BEGIN;
INSERT INTO `fc_dept` (`id`, `name`, `pid`, `status`, `desc`, `create_time`, `update_time`) VALUES (1, '技术部', 0, 1, '负责结束的', '2021-12-10 10:33:10', NULL);
INSERT INTO `fc_dept` (`id`, `name`, `pid`, `status`, `desc`, `create_time`, `update_time`) VALUES (2, '前端组', 1, 1, '负责前端的web页面', '2021-12-10 11:43:36', '2021-12-10 14:00:47');
INSERT INTO `fc_dept` (`id`, `name`, `pid`, `status`, `desc`, `create_time`, `update_time`) VALUES (3, '营销部', 0, 1, '负责市场营销', '2021-12-10 13:45:28', NULL);
COMMIT;

-- ----------------------------
-- Table structure for fc_dept_leader
-- ----------------------------
DROP TABLE IF EXISTS `fc_dept_leader`;
CREATE TABLE `fc_dept_leader` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dept_id` int(11) DEFAULT NULL COMMENT '部门id',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='部门领导表';

-- ----------------------------
-- Records of fc_dept_leader
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for fc_dict
-- ----------------------------
DROP TABLE IF EXISTS `fc_dict`;
CREATE TABLE `fc_dict` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '字典id',
  `cate_id` int(11) DEFAULT NULL COMMENT '所属分类',
  `key` varchar(155) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '键',
  `value` varchar(155) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '值',
  `status` tinyint(2) DEFAULT NULL COMMENT '状态 1:有效 2:无效',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='字典表';

-- ----------------------------
-- Records of fc_dict
-- ----------------------------
BEGIN;
INSERT INTO `fc_dict` (`id`, `cate_id`, `key`, `value`, `status`) VALUES (1, 3, '已购买', '1', 1);
INSERT INTO `fc_dict` (`id`, `cate_id`, `key`, `value`, `status`) VALUES (2, 1, '男', '1', 1);
INSERT INTO `fc_dict` (`id`, `cate_id`, `key`, `value`, `status`) VALUES (3, 1, '女', '2', 1);
INSERT INTO `fc_dict` (`id`, `cate_id`, `key`, `value`, `status`) VALUES (4, 1, '未知', '3', 1);
INSERT INTO `fc_dict` (`id`, `cate_id`, `key`, `value`, `status`) VALUES (5, 4, '红色', '11', 1);
COMMIT;

-- ----------------------------
-- Table structure for fc_dict_cate
-- ----------------------------
DROP TABLE IF EXISTS `fc_dict_cate`;
CREATE TABLE `fc_dict_cate` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '字典分类id',
  `pid` int(11) DEFAULT '0' COMMENT '上级id',
  `name` varchar(155) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '字典名称',
  `code` varchar(155) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '字典编码',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='字典分类表';

-- ----------------------------
-- Records of fc_dict_cate
-- ----------------------------
BEGIN;
INSERT INTO `fc_dict_cate` (`id`, `pid`, `name`, `code`) VALUES (1, 0, '性别', 'sex');
INSERT INTO `fc_dict_cate` (`id`, `pid`, `name`, `code`) VALUES (2, 0, '状态', 'status');
INSERT INTO `fc_dict_cate` (`id`, `pid`, `name`, `code`) VALUES (3, 2, '购买状态', 'buy');
INSERT INTO `fc_dict_cate` (`id`, `pid`, `name`, `code`) VALUES (4, 0, '类型', 'type');
COMMIT;

-- ----------------------------
-- Table structure for fc_flow_run
-- ----------------------------
DROP TABLE IF EXISTS `fc_flow_run`;
CREATE TABLE `fc_flow_run` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `flow_id` int(11) DEFAULT NULL COMMENT '所属流程的id',
  `now_step` int(11) DEFAULT '0' COMMENT '当前阶段',
  `business_id` int(11) DEFAULT NULL COMMENT '关联的业务id',
  `status` tinyint(2) DEFAULT NULL COMMENT '状态 1:待审批 2:通过 3:拒绝 4:回退 5:终止',
  `user_id` int(11) DEFAULT NULL COMMENT '发起人id',
  `user_name` varchar(155) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '发起人姓名',
  `use_flow_data` longtext COLLATE utf8mb4_bin COMMENT '使用的解析后的流程',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `idx_business_id` (`business_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='流程流转表';

-- ----------------------------
-- Records of fc_flow_run
-- ----------------------------
BEGIN;
INSERT INTO `fc_flow_run` (`id`, `flow_id`, `now_step`, `business_id`, `status`, `user_id`, `user_name`, `use_flow_data`, `create_time`, `update_time`) VALUES (1, 1, 0, 1, 1, 1, '管理员', '[[{\"id\":\"d4860ff6-32ef-4c6d-9f12-8901748439a8\",\"type\":\"taskNode\",\"data\":{\"type\":\"taskNode\",\"name\":\"\\u6280\\u672f\\u90e8\\u5ba1\\u6279\",\"desc\":\"\\u6d41\\u7a0b\\u8282\\u70b9\",\"executor_type\":\"1\",\"executor\":[[2]],\"sign_type\":1,\"can_back_off\":\"1\",\"can_end\":\"1\",\"can_transfer\":\"1\",\"can_add_signer\":\"1\",\"real_data\":[{\"id\":2,\"name\":\"\\u5ba2\\u670d2\"}]}},{\"id\":\"aa4dfa22-abf8-4873-8220-0e050489ff2c\",\"type\":\"taskNode\",\"data\":{\"type\":\"taskNode\",\"name\":\"\\u8fd0\\u8425\\u90e8\\u5ba1\\u6279\",\"desc\":\"\\u6d41\\u7a0b\\u8282\\u70b9\",\"executor_type\":\"2\",\"executor\":[[1,7]],\"sign_type\":1,\"can_back_off\":\"1\",\"can_end\":\"1\",\"can_transfer\":\"1\",\"can_add_signer\":\"1\",\"real_data\":[{\"id\":1,\"name\":\"\\u7ba1\\u7406\\u5458\",\"avatar\":\"https:\\/\\/www.cizhua.com\\/static\\/images\\/touxiang.png\"},{\"id\":7,\"name\":\"dada\",\"avatar\":\"https:\\/\\/www.cizhua.com\\/static\\/images\\/touxiang.png\"}]}}],[{\"id\":\"7efcd4b4-494c-4ad6-a0ab-4059c7c32edd\",\"type\":\"taskNode\",\"data\":{\"type\":\"taskNode\",\"name\":\"\\u8d22\\u52a1\\u5ba1\\u6279\",\"desc\":\"\\u6d41\\u7a0b\\u8282\\u70b9\",\"executor_type\":\"3\",\"executor\":[[3]],\"sign_type\":1,\"can_back_off\":\"1\",\"can_end\":\"1\",\"can_transfer\":\"1\",\"can_add_signer\":\"1\",\"real_data\":[{\"id\":3,\"name\":\"\\u8425\\u9500\\u90e8\"}]}},{\"id\":\"51dacf93-df4c-4504-b783-f751dde48299\",\"type\":\"ccNode\",\"data\":{\"type\":\"ccNode\",\"name\":\"\\u6284\\u9001\\u8282\\u70b9\",\"cc_type\":\"2\",\"ccPerson\":[[1,7]],\"real_data\":[{\"id\":1,\"name\":\"\\u7ba1\\u7406\\u5458\",\"avatar\":\"https:\\/\\/www.cizhua.com\\/static\\/images\\/touxiang.png\"},{\"id\":7,\"name\":\"dada\",\"avatar\":\"https:\\/\\/www.cizhua.com\\/static\\/images\\/touxiang.png\"}]}}],[{\"id\":\"46dbe284-b239-4a17-9096-1927c2292e95\",\"type\":\"endNode\",\"data\":[]}]]', '2022-05-04 22:26:58', NULL);
COMMIT;

-- ----------------------------
-- Table structure for fc_flow_run_detail
-- ----------------------------
DROP TABLE IF EXISTS `fc_flow_run_detail`;
CREATE TABLE `fc_flow_run_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `flow_id` int(11) DEFAULT NULL COMMENT '所属流程id',
  `flow_run_id` int(11) DEFAULT NULL COMMENT '所属流程流转id',
  `type` varchar(15) COLLATE utf8mb4_bin DEFAULT 'taskNode' COMMENT '类型 审批节点:taskNode 抄送节点 ccNode',
  `executor_id` int(11) DEFAULT NULL COMMENT '审批人id',
  `executor_name` varchar(155) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '审批人姓名',
  `sign_type` tinyint(2) DEFAULT '1' COMMENT '审核类型 1:会签 2:或签',
  `can_back_off` tinyint(2) DEFAULT '1' COMMENT '是否可以回退 1:不可以 2:可以',
  `can_end` tinyint(2) DEFAULT '1' COMMENT '是否可以终止 1:不可以 2:可以',
  `can_transfer` tinyint(2) DEFAULT '1' COMMENT '是否可以转交 1:不可以 2:可以',
  `can_add_signer` tinyint(2) DEFAULT '1' COMMENT '是否允许加签 1:不可以 2:可以',
  `status` tinyint(2) DEFAULT '1' COMMENT '状态 1:待审批 2:通过 3:拒绝 4:回退 5:终止 6:转交 7:加签',
  `create_time` datetime DEFAULT NULL COMMENT '添加时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='流程流转详情';

-- ----------------------------
-- Records of fc_flow_run_detail
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for fc_form
-- ----------------------------
DROP TABLE IF EXISTS `fc_form`;
CREATE TABLE `fc_form` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '表单id',
  `title` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '表单名称',
  `table` varchar(155) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '表名',
  `now_version` int(11) DEFAULT '1' COMMENT '当前版本',
  `deploy_version` int(11) DEFAULT '0' COMMENT '部署版本',
  `status` tinyint(2) DEFAULT '1' COMMENT '部署状态 1 未部署 2 已部署',
  `form_json` longtext COLLATE utf8mb4_bin COMMENT '设计元素',
  `conf_json` longtext COLLATE utf8mb4_bin COMMENT '生成配置文件',
  `sub_table_num` int(11) DEFAULT '0' COMMENT '子表数量',
  `sub_table_key` text COLLATE utf8mb4_bin COMMENT '子表的唯一标识',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '编辑时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='表单设计表';

-- ----------------------------
-- Records of fc_form
-- ----------------------------
BEGIN;
INSERT INTO `fc_form` (`id`, `title`, `table`, `now_version`, `deploy_version`, `status`, `form_json`, `conf_json`, `sub_table_num`, `sub_table_key`, `create_time`, `update_time`) VALUES (1, '我的测试', 'test', 4, 4, 1, '{\"column\":[{\"type\":\"input\",\"label\":\"\\u59d3\\u540d\",\"span\":24,\"display\":true,\"prop\":\"f_xjk5uhr1bbw7\"}],\"labelPosition\":\"left\",\"labelSuffix\":\"\\uff1a\",\"labelWidth\":120,\"gutter\":0,\"menuBtn\":true,\"submitBtn\":true,\"submitText\":\"\\u63d0\\u4ea4\",\"emptyBtn\":true,\"emptyText\":\"\\u6e05\\u7a7a\",\"menuPosition\":\"center\",\"tabs\":true,\"group\":[{\"label\":\"\\u5408\\u540c\\u4fe1\\u606f\",\"prop\":\"f_1n15uhr1cxqq\",\"arrow\":false,\"collapse\":true,\"display\":true,\"column\":[{\"type\":\"dynamic\",\"label\":\"\",\"span\":24,\"display\":true,\"children\":{\"align\":\"center\",\"headerAlign\":\"center\",\"index\":false,\"addBtn\":true,\"delBtn\":true,\"column\":[{\"type\":\"input\",\"label\":\"\\u5408\\u540c\\u7f16\\u53f7\",\"span\":24,\"display\":true,\"prop\":\"f_6v95uhr1jd2w\"},{\"type\":\"input\",\"label\":\"\\u91d1\\u989d\",\"span\":24,\"display\":true,\"prop\":\"f_hub5uhr1pjji\"},{\"type\":\"checkbox\",\"label\":\"\\u5408\\u4f5c\\u65b9\\u5f0f\",\"dicData\":[{\"label\":\"\\u9009\\u9879\\u4e00\",\"value\":\"0\"},{\"label\":\"\\u9009\\u9879\\u4e8c\",\"value\":\"1\"},{\"label\":\"\\u9009\\u9879\\u4e09\",\"value\":\"2\"}],\"span\":24,\"display\":true,\"props\":{\"label\":\"label\",\"value\":\"value\"},\"prop\":\"f_osh5uhyb71re\"}]},\"prop\":\"f_uq95uhr1dbuk\"}]},{\"label\":\"\\u5546\\u673a\\u4fe1\\u606f\",\"prop\":\"f_k235uhr2bhn1\",\"arrow\":false,\"collapse\":true,\"display\":true,\"column\":[{\"type\":\"dynamic\",\"label\":\"\",\"span\":24,\"display\":true,\"children\":{\"align\":\"center\",\"headerAlign\":\"center\",\"index\":false,\"addBtn\":true,\"delBtn\":true,\"column\":[{\"type\":\"input\",\"label\":\"\\u5546\\u673a\\u7f16\\u53f7\",\"span\":24,\"display\":true,\"prop\":\"f_pen5uhr1z0jj\"},{\"type\":\"input\",\"label\":\"\\u5546\\u673a\\u540d\\u79f0\",\"span\":24,\"display\":true,\"prop\":\"f_agw5uhr1zjp8\"}]},\"prop\":\"f_mh95uhr1hteh\"}]}]}', NULL, 0, '', '2022-04-09 21:37:25', '2022-04-18 22:40:12');
INSERT INTO `fc_form` (`id`, `title`, `table`, `now_version`, `deploy_version`, `status`, `form_json`, `conf_json`, `sub_table_num`, `sub_table_key`, `create_time`, `update_time`) VALUES (2, '报销申请', 'baoxiao', 7, 8, 2, '{\"column\":[{\"type\":\"input\",\"label\":\"\\u7533\\u8bf7\\u4eba\",\"span\":24,\"display\":true,\"prop\":\"f_xor5un6y7p6f\"},{\"type\":\"input\",\"label\":\"\\u62a5\\u9500\\u91d1\\u989d\",\"span\":24,\"display\":true,\"prop\":\"f_5ae5un6y9pjq\"},{\"type\":\"select\",\"label\":\"\\u62a5\\u9500\\u7c7b\\u578b\",\"dicData\":[{\"label\":\"\\u529e\\u516c\",\"value\":\"0\"},{\"label\":\"\\u5dee\\u65c5\",\"value\":\"1\"},{\"label\":\"\\u91c7\\u8d2d\",\"value\":\"2\"},{\"label\":\"\\u65e5\\u5e38\",\"value\":\"3\"}],\"cascaderItem\":[],\"span\":24,\"display\":true,\"props\":{\"label\":\"label\",\"value\":\"value\"},\"prop\":\"f_jb35un6yc9q3\"},{\"type\":\"date\",\"label\":\"\\u53d1\\u751f\\u65f6\\u95f4\",\"span\":24,\"display\":true,\"format\":\"yyyy-MM-dd\",\"valueFormat\":\"yyyy-MM-dd\",\"prop\":\"f_2rg5un6yo7oq\"}],\"labelPosition\":\"left\",\"labelSuffix\":\"\\uff1a\",\"labelWidth\":120,\"gutter\":0,\"menuBtn\":true,\"submitBtn\":true,\"submitText\":\"\\u63d0\\u4ea4\",\"emptyBtn\":true,\"emptyText\":\"\\u6e05\\u7a7a\",\"menuPosition\":\"center\"}', NULL, 0, '[]', '2022-04-09 21:47:38', '2022-04-23 14:46:15');
COMMIT;

-- ----------------------------
-- Table structure for fc_node
-- ----------------------------
DROP TABLE IF EXISTS `fc_node`;
CREATE TABLE `fc_node` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '角色id',
  `node_name` varchar(55) CHARACTER SET utf8 DEFAULT NULL COMMENT '节点名称',
  `flag` varchar(155) COLLATE utf8mb4_bin DEFAULT 'system' COMMENT '菜单标识',
  `web_path` varchar(155) CHARACTER SET utf8 DEFAULT NULL COMMENT '前端显示路由',
  `node_path` varchar(55) CHARACTER SET utf8 DEFAULT NULL COMMENT '节点路径',
  `component` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '加载的模板',
  `node_pid` int(11) DEFAULT NULL COMMENT '所属节点',
  `node_icon` varchar(55) CHARACTER SET utf8 DEFAULT NULL COMMENT '节点图标',
  `is_menu` tinyint(1) DEFAULT '1' COMMENT '是否是菜单项 1 不是 2 是',
  `sort` int(4) DEFAULT '0' COMMENT '排序，值越大越靠前',
  `add_time` datetime DEFAULT NULL COMMENT '添加时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPACT COMMENT='节点表';

-- ----------------------------
-- Records of fc_node
-- ----------------------------
BEGIN;
INSERT INTO `fc_node` (`id`, `node_name`, `flag`, `web_path`, `node_path`, `component`, `node_pid`, `node_icon`, `is_menu`, `sort`, `add_time`, `update_time`) VALUES (1, '系统管理', 'system', '/sys', '#', '#', 0, 'el-icon-setting', 2, 0, '2021-12-08 15:19:02', NULL);
INSERT INTO `fc_node` (`id`, `node_name`, `flag`, `web_path`, `node_path`, `component`, `node_pid`, `node_icon`, `is_menu`, `sort`, `add_time`, `update_time`) VALUES (2, '员工管理', 'system', '/user/index', 'user/index', 'user', 1, '', 2, 0, '2021-12-08 15:20:07', NULL);
INSERT INTO `fc_node` (`id`, `node_name`, `flag`, `web_path`, `node_path`, `component`, `node_pid`, `node_icon`, `is_menu`, `sort`, `add_time`, `update_time`) VALUES (3, '新增员工', 'system', NULL, 'user/add', NULL, 2, NULL, 1, 0, '2021-12-08 15:57:23', NULL);
INSERT INTO `fc_node` (`id`, `node_name`, `flag`, `web_path`, `node_path`, `component`, `node_pid`, `node_icon`, `is_menu`, `sort`, `add_time`, `update_time`) VALUES (4, '编辑员工', 'system', NULL, 'user/edit', NULL, 2, NULL, 1, 0, '2021-12-08 15:57:49', NULL);
INSERT INTO `fc_node` (`id`, `node_name`, `flag`, `web_path`, `node_path`, `component`, `node_pid`, `node_icon`, `is_menu`, `sort`, `add_time`, `update_time`) VALUES (5, '删除员工', 'system', NULL, 'user/del', NULL, 2, NULL, 1, 0, '2021-12-08 15:58:09', NULL);
INSERT INTO `fc_node` (`id`, `node_name`, `flag`, `web_path`, `node_path`, `component`, `node_pid`, `node_icon`, `is_menu`, `sort`, `add_time`, `update_time`) VALUES (6, '角色管理', 'system', '/role/index', 'role/index', 'role', 1, '', 2, 0, '2021-12-08 15:59:20', NULL);
INSERT INTO `fc_node` (`id`, `node_name`, `flag`, `web_path`, `node_path`, `component`, `node_pid`, `node_icon`, `is_menu`, `sort`, `add_time`, `update_time`) VALUES (7, '添加角色', 'system', NULL, 'role/add', NULL, 6, NULL, 1, 0, '2021-12-08 16:01:29', NULL);
INSERT INTO `fc_node` (`id`, `node_name`, `flag`, `web_path`, `node_path`, `component`, `node_pid`, `node_icon`, `is_menu`, `sort`, `add_time`, `update_time`) VALUES (8, '编辑角色', 'system', NULL, 'role/edit', NULL, 6, NULL, 1, 0, '2021-12-08 16:01:33', NULL);
INSERT INTO `fc_node` (`id`, `node_name`, `flag`, `web_path`, `node_path`, `component`, `node_pid`, `node_icon`, `is_menu`, `sort`, `add_time`, `update_time`) VALUES (9, '删除角色', 'system', NULL, 'role/del', NULL, 6, NULL, 1, 0, '2021-12-08 16:01:37', NULL);
INSERT INTO `fc_node` (`id`, `node_name`, `flag`, `web_path`, `node_path`, `component`, `node_pid`, `node_icon`, `is_menu`, `sort`, `add_time`, `update_time`) VALUES (10, '部门管理', 'system', '/dept/index', 'dept/index', 'dept', 1, '', 2, 0, '2021-12-08 16:05:54', NULL);
INSERT INTO `fc_node` (`id`, `node_name`, `flag`, `web_path`, `node_path`, `component`, `node_pid`, `node_icon`, `is_menu`, `sort`, `add_time`, `update_time`) VALUES (11, '新增部门', 'system', NULL, 'dept/add', NULL, 10, NULL, 1, 0, '2021-12-08 16:06:18', NULL);
INSERT INTO `fc_node` (`id`, `node_name`, `flag`, `web_path`, `node_path`, `component`, `node_pid`, `node_icon`, `is_menu`, `sort`, `add_time`, `update_time`) VALUES (12, '编辑部门', 'system', NULL, 'dept/edit', NULL, 10, NULL, 1, 0, '2021-12-08 16:06:40', NULL);
INSERT INTO `fc_node` (`id`, `node_name`, `flag`, `web_path`, `node_path`, `component`, `node_pid`, `node_icon`, `is_menu`, `sort`, `add_time`, `update_time`) VALUES (13, '删除部门', 'system', NULL, 'dept/del', NULL, 10, NULL, 1, 0, '2021-12-08 16:06:58', NULL);
INSERT INTO `fc_node` (`id`, `node_name`, `flag`, `web_path`, `node_path`, `component`, `node_pid`, `node_icon`, `is_menu`, `sort`, `add_time`, `update_time`) VALUES (14, '字典配置', 'system', '/dict/index', 'dict/index', 'dict', 1, '', 2, 0, '2021-12-12 10:11:11', NULL);
INSERT INTO `fc_node` (`id`, `node_name`, `flag`, `web_path`, `node_path`, `component`, `node_pid`, `node_icon`, `is_menu`, `sort`, `add_time`, `update_time`) VALUES (15, '字典分类树', 'system', '/dictcate', 'dictcate/index', 'dictcate', 14, NULL, 1, 0, '2021-12-12 10:12:17', NULL);
INSERT INTO `fc_node` (`id`, `node_name`, `flag`, `web_path`, `node_path`, `component`, `node_pid`, `node_icon`, `is_menu`, `sort`, `add_time`, `update_time`) VALUES (16, '添加字典分类', 'system', NULL, 'dictcate/add', NULL, 15, NULL, 1, 0, '2021-12-12 10:13:04', NULL);
INSERT INTO `fc_node` (`id`, `node_name`, `flag`, `web_path`, `node_path`, `component`, `node_pid`, `node_icon`, `is_menu`, `sort`, `add_time`, `update_time`) VALUES (17, '编辑字典分类', 'system', NULL, 'dictcate/edit', NULL, 15, NULL, 1, 0, '2021-12-12 10:13:24', NULL);
INSERT INTO `fc_node` (`id`, `node_name`, `flag`, `web_path`, `node_path`, `component`, `node_pid`, `node_icon`, `is_menu`, `sort`, `add_time`, `update_time`) VALUES (18, '删除字典分类', 'system', NULL, 'dictcate/del', NULL, 15, NULL, 1, 0, '2021-12-12 10:13:45', NULL);
INSERT INTO `fc_node` (`id`, `node_name`, `flag`, `web_path`, `node_path`, `component`, `node_pid`, `node_icon`, `is_menu`, `sort`, `add_time`, `update_time`) VALUES (19, '添加字典项', 'system', NULL, 'dict/add', NULL, 14, NULL, 1, 0, '2021-12-12 10:15:30', NULL);
INSERT INTO `fc_node` (`id`, `node_name`, `flag`, `web_path`, `node_path`, `component`, `node_pid`, `node_icon`, `is_menu`, `sort`, `add_time`, `update_time`) VALUES (20, '编辑字典项', 'system', NULL, 'dict/edit', NULL, 14, NULL, 1, 0, '2021-12-12 10:15:26', NULL);
INSERT INTO `fc_node` (`id`, `node_name`, `flag`, `web_path`, `node_path`, `component`, `node_pid`, `node_icon`, `is_menu`, `sort`, `add_time`, `update_time`) VALUES (21, '删除字典项', 'system', NULL, 'dict/del', NULL, 14, NULL, 1, 0, '2021-12-12 10:15:50', NULL);
INSERT INTO `fc_node` (`id`, `node_name`, `flag`, `web_path`, `node_path`, `component`, `node_pid`, `node_icon`, `is_menu`, `sort`, `add_time`, `update_time`) VALUES (22, '低代码开发', 'system', '/online', '#', '#', 0, 'el-icon-cloudy', 2, 40, '2021-12-13 17:00:37', NULL);
INSERT INTO `fc_node` (`id`, `node_name`, `flag`, `web_path`, `node_path`, `component`, `node_pid`, `node_icon`, `is_menu`, `sort`, `add_time`, `update_time`) VALUES (23, '表单设计', 'system', '/form/index', 'form/index', 'form', 22, '', 2, 40, '2021-12-13 17:01:11', NULL);
INSERT INTO `fc_node` (`id`, `node_name`, `flag`, `web_path`, `node_path`, `component`, `node_pid`, `node_icon`, `is_menu`, `sort`, `add_time`, `update_time`) VALUES (24, '新增表单', 'system', NULL, 'form/add', NULL, 22, NULL, 1, 40, '2022-01-06 22:16:18', NULL);
INSERT INTO `fc_node` (`id`, `node_name`, `flag`, `web_path`, `node_path`, `component`, `node_pid`, `node_icon`, `is_menu`, `sort`, `add_time`, `update_time`) VALUES (25, '编辑表单', 'system', NULL, 'form/edit', NULL, 22, NULL, 1, 40, '2022-01-06 22:16:43', NULL);
INSERT INTO `fc_node` (`id`, `node_name`, `flag`, `web_path`, `node_path`, `component`, `node_pid`, `node_icon`, `is_menu`, `sort`, `add_time`, `update_time`) VALUES (26, '删除表单', 'system', NULL, 'form/del', NULL, 22, NULL, 1, 40, '2022-01-06 22:17:01', NULL);
INSERT INTO `fc_node` (`id`, `node_name`, `flag`, `web_path`, `node_path`, `component`, `node_pid`, `node_icon`, `is_menu`, `sort`, `add_time`, `update_time`) VALUES (27, '部署表单', 'system', NULL, 'form/deploy', NULL, 22, NULL, 1, 40, '2022-01-10 11:07:51', NULL);
INSERT INTO `fc_node` (`id`, `node_name`, `flag`, `web_path`, `node_path`, `component`, `node_pid`, `node_icon`, `is_menu`, `sort`, `add_time`, `update_time`) VALUES (28, '卸载表单', 'system', NULL, 'form/undeploy', NULL, 22, NULL, 1, 40, '2022-01-19 13:33:09', NULL);
INSERT INTO `fc_node` (`id`, `node_name`, `flag`, `web_path`, `node_path`, `component`, `node_pid`, `node_icon`, `is_menu`, `sort`, `add_time`, `update_time`) VALUES (58, '流程管理', 'system', '/flow', '#', '#', 0, 'el-icon-share', 2, 39, '2022-04-13 21:55:30', NULL);
INSERT INTO `fc_node` (`id`, `node_name`, `flag`, `web_path`, `node_path`, `component`, `node_pid`, `node_icon`, `is_menu`, `sort`, `add_time`, `update_time`) VALUES (59, '流程设计', 'system', '/flow/index', 'flow/index', 'flow', 58, NULL, 2, 39, '2022-04-13 21:56:35', NULL);
INSERT INTO `fc_node` (`id`, `node_name`, `flag`, `web_path`, `node_path`, `component`, `node_pid`, `node_icon`, `is_menu`, `sort`, `add_time`, `update_time`) VALUES (60, '新增流程', 'system', '/flow/add', 'flow/add', '', 59, NULL, 1, 39, '2022-04-13 21:57:15', NULL);
INSERT INTO `fc_node` (`id`, `node_name`, `flag`, `web_path`, `node_path`, `component`, `node_pid`, `node_icon`, `is_menu`, `sort`, `add_time`, `update_time`) VALUES (61, '编辑流程', 'system', '/flow/edit', 'flow/edit', NULL, 59, NULL, 1, 39, '2022-04-13 21:57:52', NULL);
INSERT INTO `fc_node` (`id`, `node_name`, `flag`, `web_path`, `node_path`, `component`, `node_pid`, `node_icon`, `is_menu`, `sort`, `add_time`, `update_time`) VALUES (63, '发布流程', 'system', '/flow/deploy', 'flow/deploy', NULL, 59, NULL, 1, 39, '2022-04-23 15:54:38', NULL);
INSERT INTO `fc_node` (`id`, `node_name`, `flag`, `web_path`, `node_path`, `component`, `node_pid`, `node_icon`, `is_menu`, `sort`, `add_time`, `update_time`) VALUES (64, '卸载流程', 'system', '/flow/undeploy', 'flow/undeploy', NULL, 59, NULL, 1, 39, '2022-04-23 15:55:28', NULL);
INSERT INTO `fc_node` (`id`, `node_name`, `flag`, `web_path`, `node_path`, `component`, `node_pid`, `node_icon`, `is_menu`, `sort`, `add_time`, `update_time`) VALUES (71, '报销申请', 'diy', '/design/baoxiao', '/curd/index?id=2', 'diy_2', 0, 'el-icon-cloudy', 2, 0, '2022-04-23 14:46:15', NULL);
INSERT INTO `fc_node` (`id`, `node_name`, `flag`, `web_path`, `node_path`, `component`, `node_pid`, `node_icon`, `is_menu`, `sort`, `add_time`, `update_time`) VALUES (72, '新增', 'diy', '', '/curd/add?id=2', '', 71, '', 1, 0, '2022-04-23 14:46:15', NULL);
INSERT INTO `fc_node` (`id`, `node_name`, `flag`, `web_path`, `node_path`, `component`, `node_pid`, `node_icon`, `is_menu`, `sort`, `add_time`, `update_time`) VALUES (73, '编辑', 'diy', '', '/curd/edit?id=2', '', 71, '', 1, 0, '2022-04-23 14:46:15', NULL);
INSERT INTO `fc_node` (`id`, `node_name`, `flag`, `web_path`, `node_path`, `component`, `node_pid`, `node_icon`, `is_menu`, `sort`, `add_time`, `update_time`) VALUES (74, '删除', 'diy', '', '/curd/del?id=2', '', 71, '', 1, 0, '2022-04-23 14:46:15', NULL);
COMMIT;

-- ----------------------------
-- Table structure for fc_role
-- ----------------------------
DROP TABLE IF EXISTS `fc_role`;
CREATE TABLE `fc_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '角色id',
  `name` varchar(55) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '角色名称',
  `role_node` text CHARACTER SET utf8mb4 COMMENT '角色拥有的菜单节点',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态  1 有效 2 无效',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='角色表';

-- ----------------------------
-- Records of fc_role
-- ----------------------------
BEGIN;
INSERT INTO `fc_role` (`id`, `name`, `role_node`, `status`, `create_time`, `update_time`) VALUES (1, '超级管理员', '*', 1, '2021-12-06 16:21:14', NULL);
INSERT INTO `fc_role` (`id`, `name`, `role_node`, `status`, `create_time`, `update_time`) VALUES (2, '客服2', '2,3,4,5,10,11,12,13,1', 1, '2021-12-08 22:22:10', '2021-12-09 13:41:38');
INSERT INTO `fc_role` (`id`, `name`, `role_node`, `status`, `create_time`, `update_time`) VALUES (3, '新的角色', '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21', 1, '2022-01-05 14:50:59', '2022-01-05 14:51:27');
COMMIT;

-- ----------------------------
-- Table structure for fc_user
-- ----------------------------
DROP TABLE IF EXISTS `fc_user`;
CREATE TABLE `fc_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '管理员id',
  `name` varchar(155) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '登录名称',
  `nickname` varchar(155) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '昵称',
  `password` varchar(155) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '管理员密码',
  `salt` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '当前用户加密盐',
  `avatar` varchar(155) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '管理员头像',
  `dept_id` int(11) DEFAULT '0' COMMENT '部门id',
  `role_id` int(11) DEFAULT '0' COMMENT '所属角色id',
  `last_login_ip` varchar(55) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '最近一次登录ip',
  `last_login_time` datetime DEFAULT NULL COMMENT '最近一次登录时间',
  `last_login_agent` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '最近一次登录设备',
  `status` tinyint(2) DEFAULT '1' COMMENT '1 正常 2 禁用',
  `is_leader` tinyint(2) DEFAULT NULL COMMENT '是否是领导 1:是 2:否',
  `leader_id` int(11) DEFAULT '0' COMMENT '直属上级id',
  `create_time` datetime DEFAULT NULL COMMENT '添加时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='用户表';

-- ----------------------------
-- Records of fc_user
-- ----------------------------
BEGIN;
INSERT INTO `fc_user` (`id`, `name`, `nickname`, `password`, `salt`, `avatar`, `dept_id`, `role_id`, `last_login_ip`, `last_login_time`, `last_login_agent`, `status`, `is_leader`, `leader_id`, `create_time`, `update_time`) VALUES (1, 'admin', '管理员', 'bcf0247013a6844d6df93df32db4437b', '61add43e50a51', 'https://www.cizhua.com/static/images/touxiang.png', 0, 1, '127.0.0.1', '2022-05-02 22:47:27', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36', 1, 2, 0, '2021-12-06 16:36:47', NULL);
INSERT INTO `fc_user` (`id`, `name`, `nickname`, `password`, `salt`, `avatar`, `dept_id`, `role_id`, `last_login_ip`, `last_login_time`, `last_login_agent`, `status`, `is_leader`, `leader_id`, `create_time`, `update_time`) VALUES (4, 'nick', '李四', '02f4f3b543120f563a37bf8b3580abd4', '61b4b684affaa', 'https://www.cizhua.com/static/images/touxiang.png', 3, 2, NULL, NULL, NULL, 1, 2, NULL, '2021-12-11 22:32:36', '2021-12-12 17:20:59');
INSERT INTO `fc_user` (`id`, `name`, `nickname`, `password`, `salt`, `avatar`, `dept_id`, `role_id`, `last_login_ip`, `last_login_time`, `last_login_agent`, `status`, `is_leader`, `leader_id`, `create_time`, `update_time`) VALUES (5, 'huang', '阿黄', '409c5701c397f7b09c80858f95a86645', '61b4beda9e8e0', 'https://www.cizhua.com/static/images/touxiang.png', 2, 2, NULL, NULL, NULL, 1, 2, 0, '2021-12-11 22:35:38', '2021-12-11 23:08:44');
INSERT INTO `fc_user` (`id`, `name`, `nickname`, `password`, `salt`, `avatar`, `dept_id`, `role_id`, `last_login_ip`, `last_login_time`, `last_login_agent`, `status`, `is_leader`, `leader_id`, `create_time`, `update_time`) VALUES (6, 'lisi', '李四', 'b89391d4e696342e672fa6535220b7da', '61b6a3ce4e76c', 'https://www.cizhua.com/static/images/touxiang.png', 3, 2, NULL, NULL, NULL, 1, 2, 5, '2021-12-13 09:37:18', NULL);
INSERT INTO `fc_user` (`id`, `name`, `nickname`, `password`, `salt`, `avatar`, `dept_id`, `role_id`, `last_login_ip`, `last_login_time`, `last_login_agent`, `status`, `is_leader`, `leader_id`, `create_time`, `update_time`) VALUES (7, 'iqeqw', 'dada', 'd6ed5028b8cb4df64d7fccffccae162f', '61d40bce0d106', 'https://www.cizhua.com/static/images/touxiang.png', 1, 2, NULL, NULL, NULL, 1, 2, 4, '2022-01-04 16:56:46', NULL);
INSERT INTO `fc_user` (`id`, `name`, `nickname`, `password`, `salt`, `avatar`, `dept_id`, `role_id`, `last_login_ip`, `last_login_time`, `last_login_agent`, `status`, `is_leader`, `leader_id`, `create_time`, `update_time`) VALUES (8, 'rerer', '去问问去', '4273b5da936604ef3969d17578ea5767', '61d40e11df311', 'https://www.cizhua.com/static/images/touxiang.png', 3, 2, NULL, NULL, NULL, 1, 2, 7, '2022-01-04 17:06:25', NULL);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
